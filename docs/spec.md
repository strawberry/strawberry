# The Strawberry protocol specification 🍓

First of all, please note that **this is an incomplete draft.** Parts of this specification may change as needed without any prior notice. This is not stable and production-ready yet.

## Core concepts and definitions

### Your key is your identity

Not unlike many other decentralized pubsub protocols, your identity on the network is defined not by your username and/or preferred server, but instead by your digital signature keypair. Strawberry MUST use RSA for this with at least 4096 bit keys. 

### Message radius

The message radius value is used to control the spread of the message across the network.

Each time it gets re-broadcasted, the radius value MUST be decreased by one, and once the value reaches zero it MUST NOT be re-broadcasted.

### Network channels

Network channels allow the network to be neatly split into multiple channels which can be subscribed to by users.

For example, instead of posting on the default channel, you could make a post on the `test` channel, which would mean that only people and relays who subscribe to the `test` channel will be able to see your post.

See the section about the Message object for more information.

### Serialization

For message transports, messages can be serialized using any serialization format that the transport method supports. However, MessagePack is the cannonical serialization format and MUST be used for signature payloads.

When serializing in JSON format, timestamps MUST be represented as an RFC3339 string and binary data MUST be represented as Base64 strings.

When serializing in MessagePack format, timestamps MUST be represented as defined [here](https://github.com/msgpack/msgpack/blob/master/spec.md#timestamp-extension-type) and binary data MUST be represented as defined [here](https://github.com/msgpack/msgpack/blob/master/spec.md#bin-format-family).

We'll provide JSON-serialized examples and Go structs in this specification for simplicity though.

<!-- This is where the fun really begins. -->

### Objects

Schema:

```go
type Object struct {
	Type string `json:"type" msgpack:"type"`
	Data any    `json:"data" msgpack:"data"`
}
```

Example:

```json
{
	"type": "strawberry:Link",
	"data": "https://example.com"
}
```

An object is the base container type used by Strawberry. Everything extends it.

The value of `type` must be in `[scope]:[name]` format. The scope and name MUST be unique and MUST NOT conflict with other uses of that type.

Type scopes MUST NOT contain characters other than lowercase letters, numbers, and underscores. Additionally, type scopes MUST start with a lowercase letter. 

Type names MUST NOT contain letters other than lowercase letters and capital letters. Additionally, type names MUST start with a capital letter.

## Object types

### SignedObject

Type value: `strawberry:SignedObject`

Schema:

```go
type SignedObject struct {
	SignatureType string `json:"signature_type" msgpack:"signature_type"`
	Signature     []byte `json:"signature" msgpack:"signature"`
	Data          Object `json:"data" msgpack:"data"`
}
```

### Message

Type value: `strawberry:Message`

Schema:

```go
type Message struct {
	Radius        uint32 `json:"radius" msgpack:"radius"`
	Channel       string `json:"channel" msgpack:"channel"`
	Data          Object `json:"data" msgpack:"data"`
}
```

NOTE: If the data is message is signed, it MUST be signed with an RSA-4096 key using PKCS1v15 and SHA-256 (the signature type string MUST be `rsa-4096-pkcs1v15-sha256`) and the message data MUST be a SignedObject.

Regarding the channel object: That's the network channel the message is intended for. Relays MUST NOT send messages to clients and relays who have not subscribed to the network channels defined in the message objects. However, relays, clients, etc. MUST interpret a blank channel value as the default channel, and relays MUST NOT require the client or relay on the other end to explicitly subscribe to the default channel.

### User

Type value: `strawberry:User`

Schema:

```go
type User struct {
	DisplayName  string       `json:"display_name" msgpack:"display_name"`
	Description  string       `json:"description" msgpack:"description"`
	SignatureKey SignatureKey `json:"signature_key" msgpack:"signature_key"`
	Mailbox      Mailbox      `json:"mailbox" msgpack:"mailbox"`
	Created      time.Time    `json:"created" msgpack:"created"`
	LastUpdated  time.Time    `json:"last_updated" msgpack:"last_updated"`
}
```

### Post

Type value: `strawberry:Post`

Schema:

```go
type Post struct {
	Author      User      `json:"author" msgpack:"author"`
	Salt        []byte    `json:"salt" msgpack:"salt"`
	CW          string    `json:"cw" msgpack:"cw"`
	Body        string    `json:"body" msgpack:"body"`
	Created     time.Time `json:"created" msgpack:"created"`
	LastUpdated time.Time `json:"last_updated" msgpack:"last_updated"`
}
```

### Link

Type value: `strawberry:Link`

Schema:

```go
type Link string
```

The link data MUST be a valid URL.