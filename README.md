# The Strawberry protocol 🍓

Communication, anywhere, anyhow, even when all else fails.

See the work-in-progress [specification](docs/spec.md) for more information.

## Isn't this a lot like nostr? Why not just use nostr?

Nostr is an interesting protocol, and it does many of the same things that Strawberry does.

However, nostr was created specifically for the internet. Strawberry is a more flexible protocol, and off-grid device-to-device communication is a major goal.

You can always set up an internet-based Strawberry relay though to extend range and/or use Strawberry on devices which cannot do device-to-device communication. And that might actually be the best option depending on your scenario.
